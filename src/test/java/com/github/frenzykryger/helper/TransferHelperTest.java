package com.github.frenzykryger.helper;

import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class TransferHelperTest {
    private static final byte[] DATA = "Buzzinga".getBytes();


    @Test
    public void testOnReadDataClosedChannel() throws IOException {
        //GIVEN
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        SocketChannel channel = mock(SocketChannel.class);
        when(channel.read(any(ByteBuffer.class))).thenReturn(-1);
        //WHEN
        TransferHelper.TransferResult result = TransferHelper.readData(channel, buffer);
        //THEN
        assertEquals(TransferHelper.TransferResult.CLOSE, result);
    }

    @Test
    public void testOnReadDataFullBuffer() throws IOException {
        //GIVEN
        ByteBuffer buffer = ByteBuffer.allocate(0);
        SocketChannel channel = mock(SocketChannel.class);
        //WHEN
        TransferHelper.TransferResult result = TransferHelper.readData(channel, buffer);
        //THEN
        verify(channel, atMost(0)).read(any(ByteBuffer.class));
        assertEquals(TransferHelper.TransferResult.SKIP, result);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testOnReadDataUngracefulClose() throws IOException {
        //GIVEN
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        SocketChannel channel = mock(SocketChannel.class);
        when(channel.read(any(ByteBuffer.class))).thenThrow(IOException.class);
        //WHEN
        TransferHelper.TransferResult result = TransferHelper.readData(channel, buffer);
        //THEN
        assertEquals(TransferHelper.TransferResult.CLOSE, result);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testOnReadDataUngracefulClose2() throws IOException {
        //GIVEN
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        SocketChannel channel = mock(SocketChannel.class);
        when(channel.read(any(ByteBuffer.class))).thenThrow(RuntimeException.class);
        //WHEN
        TransferHelper.TransferResult result = TransferHelper.readData(channel, buffer);
        //THEN
        assertEquals(TransferHelper.TransferResult.CLOSE, result);
    }

    @Test
    public void testOnWriteDataSkipEmptyBuffer() throws IOException {
        //GIVEN
        ByteBuffer buffer = ByteBuffer.allocate(0);
        SocketChannel channel = mock(SocketChannel.class);
        //WHEN
        TransferHelper.TransferResult result = TransferHelper.writeData(channel, buffer);
        //THEN
        verify(channel, atMost(0)).write(any(ByteBuffer.class));
        assertEquals(TransferHelper.TransferResult.SKIP, result);
    }

    @Test
    public void testOnWriteDataFully() throws IOException {
        //GIVEN
        ByteBuffer buffer = ByteBuffer.wrap(new byte[DATA.length]);
        buffer.put(DATA);
        SocketChannel channel = mock(SocketChannel.class);
        when(channel.write(any(ByteBuffer.class))).then(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                ByteBuffer buffer = (ByteBuffer) invocationOnMock.getArguments()[0];
                buffer.get(new byte[DATA.length]);
                return DATA.length;
            }
        });

        //WHEN
        TransferHelper.TransferResult result = TransferHelper.writeData(channel, buffer);
        //THEN
        assertEquals(TransferHelper.TransferResult.OK, result);
        assertEquals(0, buffer.position());
        assertEquals(DATA.length, buffer.limit());
    }

    @Test
    public void testOnWriteDataPartial() throws IOException {
        //GIVEN
        ByteBuffer buffer = ByteBuffer.wrap(new byte[1024]);
        buffer.put(DATA);
        SocketChannel channel = mock(SocketChannel.class);
        when(channel.write(any(ByteBuffer.class))).then(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                ByteBuffer buffer = (ByteBuffer) invocationOnMock.getArguments()[0];
                buffer.get();
                return 1;
            }
        });

        //WHEN
        TransferHelper.TransferResult result = TransferHelper.writeData(channel, buffer);
        //THEN
        assertEquals(TransferHelper.TransferResult.PARTIAL, result);
        assertEquals(DATA.length - 1, buffer.position());
        assertEquals(1024, buffer.limit());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testOnWriteDataUngracefulClose() throws IOException {
        //GIVEN
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        buffer.put(DATA);
        SocketChannel channel = mock(SocketChannel.class);
        when(channel.write(any(ByteBuffer.class))).thenThrow(IOException.class);
        //WHEN
        TransferHelper.TransferResult result = TransferHelper.writeData(channel, buffer);
        //THEN
        assertEquals(TransferHelper.TransferResult.CLOSE, result);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testOnWriteDataUngracefulClose2() throws IOException {
        //GIVEN
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        buffer.put(DATA);
        SocketChannel channel = mock(SocketChannel.class);
        when(channel.write(any(ByteBuffer.class))).thenThrow(RuntimeException.class);
        //WHEN
        TransferHelper.TransferResult result = TransferHelper.writeData(channel, buffer);
        //THEN
        assertEquals(TransferHelper.TransferResult.CLOSE, result);
    }
}
