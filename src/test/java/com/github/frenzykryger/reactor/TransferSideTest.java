package com.github.frenzykryger.reactor;

import com.github.frenzykryger.helper.ConnectionManagementHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({TransferSide.class, ConnectionManagementHelper.class})
public class TransferSideTest {

    @Test
    public void testOnTransferRegistration() throws ClosedChannelException {
        // GIVEN
        Selector selector = mock(Selector.class);
        TransferSide transfer = initializeTransfer(selector);
        // WHEN
        SelectionKey key = transfer.register(selector, null);
        // THEN
        verify(key, times(1)).attach(eq(transfer));
    }

    @Test
    public void testOnClose() throws ClosedChannelException {
        // GIVEN
        mockStatic(ConnectionManagementHelper.class);
        Selector selector = mock(Selector.class);
        TransferSide side = initializeTransfer(selector);
        TransferSide otherSide = initializeTransfer(selector);
        side.link(otherSide);
        otherSide.link(side);
        side.register(selector, null);
        otherSide.register(selector, null);
        assertFalse(otherSide.shouldBeClosed);
        // WHEN
        side.close();
        // THEN
        assertTrue(otherSide.shouldBeClosed);
    }

    private TransferSide initializeTransfer(Selector selector) throws ClosedChannelException {
        ByteBufferPool pool = new ByteBufferPool(8, 1024);
        SocketChannel channel = mock(SocketChannel.class);
        TransferSide transfer = new TransferSide(channel, null, pool);
        SelectionKey key = mock(SelectionKey.class);
        when(key.equals(any())).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return invocation.getMock() == invocation.getArguments()[0];
            }
        });
        when(key.channel()).thenReturn(channel);
        when(key.isValid()).thenReturn(true);
        when(channel.register(eq(selector), eq(SelectionKey.OP_READ))).thenReturn(key);
        return transfer;
    }
}
