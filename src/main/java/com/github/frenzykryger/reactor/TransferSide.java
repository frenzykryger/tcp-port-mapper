package com.github.frenzykryger.reactor;

import com.github.frenzykryger.configuration.PortMapping;
import com.github.frenzykryger.helper.ConnectionManagementHelper;
import com.github.frenzykryger.helper.TransferHelper;

import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

/**
 * Maintains one side of proxy connection
 * is able to provide link to other side.
 * Not thread-safe.
 */
public class TransferSide {
    private final SocketChannel channel;
    private final ByteBufferPool pool;
    private final PortMapping portMapping;
    protected ByteBuffer buffer;
    protected boolean shouldBeClosed = false;
    private SelectionKey key;
    private TransferSide otherSide;

    /**
     * Return one transfer already linked to another for two channels provided
     *
     * @param oneChannel   one channel (for transfer returned)
     * @param otherChannel channel for other transfer
     * @param portMapping  port mapping for tracking
     * @param pool         byte buffer pool
     * @return one transfer with linked another (see {getOtherSide()})
     */
    public static TransferSide makePair(SocketChannel oneChannel, SocketChannel otherChannel,
                                        PortMapping portMapping, ByteBufferPool pool) {
        TransferSide side = new TransferSide(oneChannel, portMapping, pool);
        TransferSide otherSide = new TransferSide(otherChannel, portMapping, pool);
        side.link(otherSide);
        otherSide.link(side);
        return side;
    }

    /**
     * Unregister both sides of transfer
     *
     * @param side one side
     */
    public static void unregisterAll(TransferSide side) {
        side.unregister();
        if (side.otherSide != null) {
            side.otherSide.unregister();
        }
    }

    public TransferSide(SocketChannel channel, PortMapping portMapping, ByteBufferPool pool) {
        this.channel = channel;
        this.portMapping = portMapping;
        this.pool = pool;
        this.buffer = pool.acquire();
    }

    /**
     * @return port mapping that corresponds to this transfer
     * (actually, there may be multiple transfers with one port mapping)
     */
    public PortMapping getPortMapping() {
        return portMapping;
    }

    /**
     * @return other side of the transfer
     */
    public TransferSide getOtherSide() {
        return otherSide;
    }

    /**
     * @return selection key
     */
    public SelectionKey getKey() {
        return key;
    }


    /**
     * Registers transfer on provided selector obtaining selection keys
     *
     * @param selector selector for channel multiplexing
     * @param ops      interested operations for channel, null to make guess on buffer content
     * @return keys obtained upon registration
     * @throws java.nio.channels.ClosedChannelException if one of channel was closed prematurely
     */
    public SelectionKey register(Selector selector, Integer ops) throws ClosedChannelException {
        if (ops == null) {
            ops = ((buffer.position() == 0) ? SelectionKey.OP_READ : SelectionKey.OP_WRITE);
        }
        key = channel.register(selector, ops);
        key.attach(this);
        return key;
    }

    /**
     * Writes data from this side buffer to channel
     *
     * @param stats statistics holder
     * @return true if transfer wasn't closed during write
     */
    public boolean writeData(Stats stats) {
        if (shouldBeClosed && isBufferEmpty()) {
            close();
            return false;
        }
        switch (stats.statsForWrite(TransferHelper.writeData(channel, buffer))) {
            case CLOSE: {
                close();
                return false;
            }
        }
        if (shouldBeClosed && isBufferEmpty()) {
            close();
            return false;
        }
        return true;
    }

    /**
     * Reads data from channel to OTHER SIDE buffer
     *
     * @param stats statistics holder
     * @return true if transfer wasn't closed during read
     */
    public boolean readData(Stats stats) {
        if (shouldBeClosed) {
            if (isBufferEmpty()) {
                close();
            } else {
                key.interestOps(SelectionKey.OP_WRITE);
            }
            return false;
        }

        switch (stats.statsForRead(TransferHelper.readData(channel, otherSide.buffer))) {
            case CLOSE: {
                close();
                return false;
            }
        }
        return true;
    }

    /**
     * Closes all channels for transfer (this and other side)
     */
    public void closeChannels() {
        doClose();
        if (otherSide != null) {
            otherSide.closeChannels();
        }
    }

    /**
     * Closes channel for this key and at least ensures that other part is marked for close
     */
    public void close() {
        doClose();
        if (otherSide != null) {
            otherSide.shouldBeClosed = true;
            if (!otherSide.key.isValid() || (otherSide.buffer.position() == 0)) {
                otherSide.doClose();
            }
        }
    }

    /**
     * @return true if buffer is full
     */
    public boolean isBufferFull() {
        return !buffer.hasRemaining();
    }

    /**
     * Sets appropriate interest opts for entire transfer
     * based on current buffers content
     */
    public void syncSides() {
        syncSide();
        if (otherSide != null) {
            otherSide.syncSide();
        }
    }

    /**
     * Sets appropriate interest opts for this transfer side
     * based on current buffers content
     */
    private void syncSide() {
        int ops = 0;
        if (!isBufferEmpty()) {
            ops |= SelectionKey.OP_WRITE;
        } else if (otherSide != null && !otherSide.isBufferFull()) {
            ops |= SelectionKey.OP_READ;
        }
        if (key.isValid()) {
            key.interestOps(ops);
        }
    }

    /**
     * cancel keys, release buffers, closes channel and breaks reference cycle
     */
    private void doClose() {
        if (key != null) {
            unregister();
        }
        if (buffer != null) {
            pool.release(buffer);
        }
        if (otherSide != null) {
            otherSide.otherSide = null;
        }
        buffer = null;
        key = null;
        ConnectionManagementHelper.tryClose(channel);
    }

    /**
     * Unbinds transfer from current used selector
     */
    private void unregister() {
        key.cancel();
        key.attach(null);
        key = null;
    }

    /**
     * @return true if buffer is empty
     */
    private boolean isBufferEmpty() {
        return buffer.position() == 0;
    }

    /**
     * Links transfer to other side
     *
     * @param otherSide other side
     */
    protected void link(TransferSide otherSide) {
        this.otherSide = otherSide;
    }
}
