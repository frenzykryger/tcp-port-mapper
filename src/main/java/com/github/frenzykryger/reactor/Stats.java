package com.github.frenzykryger.reactor;

import com.github.frenzykryger.helper.TransferHelper;

/**
 * Not thread-safe stats
 */
public class Stats {
    public class Read {
        private int ok;
        private int close;
        private int skip;

        @Override
        public String toString() {
            return "Read{" +
                    "ok=" + ok +
                    ", close=" + close +
                    ", skip=" + skip +
                    '}';
        }
    }

    public class Write {
        private int ok;
        private int close;
        private int skip;
        private int partial;

        @Override
        public String toString() {
            return "Write{" +
                    "ok=" + ok +
                    ", close=" + close +
                    ", skip=" + skip +
                    ", partial=" + partial +
                    '}';
        }
    }

    private Read read = new Read();
    private Write write = new Write();
    private int cnt;
    private int transfers;
    private int speculativeWrites;
    private long period = System.currentTimeMillis();

    public void reset() {
        read.ok = read.close = read.skip = write.ok = write.close = write.skip = write.partial =
                cnt = transfers = speculativeWrites = 0;
        period = System.currentTimeMillis();
    }

    public int inc() {
        return ++cnt;
    }

    public void transferAccepted() {
        ++transfers;
    }

    public void speculativeWrite() {
        ++speculativeWrites;
    }

    public TransferHelper.TransferResult statsForRead(TransferHelper.TransferResult result) {
        switch (result) {
            case OK:
                ++read.ok;
                break;
            case CLOSE:
                ++read.close;
                break;
            case SKIP:
                ++read.skip;
                break;
        }
        return result;
    }

    public TransferHelper.TransferResult statsForWrite(TransferHelper.TransferResult result) {
        switch (result) {
            case OK:
                ++write.ok;
                break;
            case CLOSE:
                ++write.close;
                break;
            case SKIP:
                ++write.skip;
                break;
            case PARTIAL:
                ++write.partial;
                break;
        }
        return result;
    }

    @Override
    public String toString() {
        return "Stats(" + cnt + ", " + (System.currentTimeMillis() - period) + " ms, " +
                "acceptedTransfers=" + transfers + ", speculativeWrites=" + speculativeWrites + "){" +
                "read=" + read +
                ", write=" + write +
                '}';
    }
}
