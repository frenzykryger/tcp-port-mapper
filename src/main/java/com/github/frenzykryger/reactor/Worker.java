package com.github.frenzykryger.reactor;

import com.github.frenzykryger.configuration.Configuration;
import com.github.frenzykryger.helper.ConnectionManagementHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Queue;

/**
 * Worker thread that actually copies data from one channel to another in selector loop (with no clue of direction).
 * Incoming transfers are accepted using incomingTransfers queue. Channels in transfer must be ready to
 * receive/transmit data.
 */
public class Worker implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(Worker.class);

    private final Selector selector;
    private final Queue<TransferSide> incomingTransfers;
    private final Configuration configuration;
    private final Stats stats = new Stats();

    /**
     * @param selector          Selector for this worker (no workers should share same selector)
     * @param incomingTransfers transfers that need to be registered in this worker
     * @param configuration     conf
     */
    public Worker(Selector selector, Queue<TransferSide> incomingTransfers, Configuration configuration) {
        this.selector = selector;
        this.incomingTransfers = incomingTransfers;
        this.configuration = configuration;
    }

    /**
     * Main selector loop
     */
    @SuppressWarnings("InfiniteLoopStatement")
    @Override
    public void run() {
        while (true) {
            try {
                work();
            } catch (IOException | RuntimeException e) {
                LOGGER.warn("Unable to do work due to exception", e);
            }
        }
    }

    private void work() throws IOException {
        if (!incomingTransfers.isEmpty()) {
            handleNewTransfers();
        }

        selector.select(configuration.getMaxWorkerIdleTimeout());

        handleNewTransfers();

        Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
        while(iterator.hasNext()) {
            SelectionKey key = iterator.next();
            iterator.remove();
            if (key.isValid()) {
                handleSelectionKey(key);
            }
        }

        if ((stats.inc() % configuration.getStatsPeriod()) == 0) {
            LOGGER.info("Worker statistics: {}", stats);
            stats.reset();
        }
    }

    private void handleNewTransfers() {
        for (int i = 0; i < configuration.getMaxTransfersChunk(); ++i) {
            TransferSide transfer = incomingTransfers.poll();
            if (transfer != null) {
                handleNewTransfer(transfer);
            } else {
                break;
            }
        }
    }

    private void handleNewTransfer(TransferSide transfer) {
        try {
            transfer.register(selector, null);
            transfer.getOtherSide().register(selector, null);
            stats.transferAccepted();
            LOGGER.debug("New transfer for {} registered", transfer.getPortMapping());
        } catch (IOException | RuntimeException e) {
            LOGGER.warn("Unable to register channel for port mapping {}", transfer.getPortMapping(), e);
            transfer.closeChannels();
        }
    }

    private void handleSelectionKey(SelectionKey key) {
        try {
            if (!key.isReadable() && !key.isWritable()) {
                LOGGER.warn("Got not OP_READ/OP_WRITE channel in worker selector. " +
                        "This shouldn't be here.");
                return;
            }

            if (key.isReadable()) {
                handleRead(key);
            }
            if (key.isValid() && key.isWritable()) {
                handleWrite(key);
            }
        } catch (RuntimeException e) {
            LOGGER.warn("Aborting transfer due to exception", e);
            if (key != null && key.isValid()) {
                key.cancel();
            }
            if (key != null) {
                TransferSide transfer = (TransferSide) key.attachment();
                if (transfer != null) {
                    transfer.closeChannels();
                }
            }
        }
    }

    private void handleWrite(SelectionKey key) {
        TransferSide transfer = (TransferSide) key.attachment();
        if (transfer != null) {
            if (transfer.writeData(stats)) {
                transfer.syncSides();
            }
        } else {
            closeOrphaned(key);
        }
    }

    private void handleRead(SelectionKey key) {
        TransferSide transfer = (TransferSide) key.attachment();
        if (transfer != null) {
            if (transfer.readData(stats)) {
                if (configuration.isSpeculativeWritesEnabled()) {
                    handleWrite(transfer.getOtherSide().getKey());
                    stats.speculativeWrite();
                } else {
                    transfer.syncSides();
                }
            }
        } else {
            closeOrphaned(key);
        }
    }

    private void closeOrphaned(SelectionKey key) {
        key.cancel();
        ConnectionManagementHelper.tryClose((SocketChannel) key.channel());
    }
}
