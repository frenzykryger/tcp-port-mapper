package com.github.frenzykryger.reactor;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Thread-safe byte buffer pool.
 * It's advised to pool byte buffers to keep memory profile constant and avoid small mallocs overhead.
 * Well at least do all small mallocs before startup.
 */
public class ByteBufferPool {
    private final ConcurrentLinkedQueue<ByteBuffer> pool = new ConcurrentLinkedQueue<>();

    public ByteBufferPool(int bufferSize, int buffersCount) {
        for (int i = 0; i < buffersCount; ++i) {
            pool.add(ByteBuffer.allocateDirect(bufferSize).order(ByteOrder.nativeOrder()));
        }
    }

    public ByteBuffer acquire() {
        ByteBuffer buffer = pool.poll();
        if (buffer == null) {
            throw new RuntimeException("ByteBufferPool exhausted");
        }
        return buffer;
    }

    public void release(ByteBuffer buffer) {
        buffer.clear();
        pool.add(buffer);
    }
}
