package com.github.frenzykryger.reactor;

import com.github.frenzykryger.configuration.Configuration;
import com.github.frenzykryger.configuration.PortMapping;
import com.github.frenzykryger.helper.ConnectionManagementHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Establishes new transfers. Then dispatches them (transferring ownership) to worker manager.
 * Transfers are dispatched only after they were unregistered from connection manager selector
 * and both channels are ready to receive/transmit data.
 */
public class ConnectionManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionManager.class);
    private final Selector connectionManagementSelector;

    private final Map<Integer, PortMapping> proxyConfiguration = new HashMap<>();

    private final WorkerManager workerManager;
    private final Configuration configuration;
    private final Stats stats = new Stats();

    /**
     * @param portMappingList port mapping from proxy.properties
     * @param workerManager   worker manager
     * @param configuration   conf
     * @throws java.lang.RuntimeException if we can't open selector
     */
    public ConnectionManager(List<PortMapping> portMappingList, WorkerManager workerManager,
                             Configuration configuration) {
        this.connectionManagementSelector = ConnectionManagementHelper.openSelector();
        for (PortMapping oneMapping : portMappingList) {
            proxyConfiguration.put(oneMapping.getLocalPort(), oneMapping);
        }
        this.workerManager = workerManager;
        this.configuration = configuration;
    }

    /**
     * Selector main loop for accepting & establishing new connections
     */
    @SuppressWarnings("InfiniteLoopStatement")
    public void acceptConnectionsIndefinitely() {
        ConnectionManagementHelper.listenOnPorts(connectionManagementSelector, proxyConfiguration.values());

        LOGGER.info("Accepting new connections");
        while (true) {
            try {
                acceptConnections();
            } catch (IOException | RuntimeException e) {
                LOGGER.warn("Error in accept new connections loop:", e);
            }
        }
    }

    private void acceptConnections() throws IOException {
        connectionManagementSelector.select();
        Iterator<SelectionKey> iterator = connectionManagementSelector.selectedKeys().iterator();
        while (iterator.hasNext()) {
            SelectionKey key = iterator.next();
            iterator.remove();
            if (key.isValid()) {
                handleKey(key);
            }
        }

        if ((stats.inc() % configuration.getStatsPeriod()) == 0) {
            LOGGER.info("Premature read statistics: {}", stats);
            stats.reset();
        }
    }

    private void handleKey(SelectionKey key) {
        if (key.isAcceptable()) {
            acceptNewConnection(key);
        } else if (key.isConnectable()) {
            establishProxyConnection(key);
        } else if (key.isReadable()) {
            doPrematureRead(key);
        } else {
            LOGGER.warn("Got not OP_ACCEPT/OP_CONNECT/OP_READ channel in connection management selector. " +
                    "This shouldn't be here.");
        }
    }

    private void acceptNewConnection(SelectionKey key) {
        ServerSocketChannel listeningChannel = (ServerSocketChannel) key.channel();
        PortMapping portMapping = proxyConfiguration.get(listeningChannel.socket().getLocalPort());
        SocketChannel localChannel = null;
        SocketChannel remoteChannel = null;
        try {
            localChannel = ConnectionManagementHelper.acceptConnection(listeningChannel);
            remoteChannel = ConnectionManagementHelper.openRemoteChannel(portMapping);

            LOGGER.debug("Accepted connection for {}", portMapping);
            TransferSide localSide = TransferSide.makePair(localChannel, remoteChannel, portMapping,
                    configuration.getByteBufferPool());

            localSide.register(connectionManagementSelector, SelectionKey.OP_READ);
            localSide.getOtherSide().register(connectionManagementSelector, SelectionKey.OP_CONNECT);
        } catch (IOException | RuntimeException e) {
            LOGGER.warn("Failed to establish new proxy channel for port mapping {}", portMapping, e);
            if (localChannel != null) {
                ConnectionManagementHelper.tryClose(localChannel);
            }
            if (remoteChannel != null) {
                ConnectionManagementHelper.tryClose(remoteChannel);
            }
        }
    }

    private void establishProxyConnection(SelectionKey key) {
        SocketChannel channel = (SocketChannel) key.channel();
        TransferSide establishedTransfer = (TransferSide) key.attachment();
        try {
            if (channel != null && channel.finishConnect()) {
                TransferSide.unregisterAll(establishedTransfer);
                // remove channels from connectionManagementSelector
                // moving here means that local channel
                // was already accepted and both channels are ready to transmit data

                if (establishedTransfer != null) {
                    LOGGER.debug("Established transfer for {}", establishedTransfer.getPortMapping());
                    workerManager.addTransfer(establishedTransfer);
                } else {
                    LOGGER.warn("Got orphaned connection with no transfer. This shouldn't happen");
                    ConnectionManagementHelper.tryClose(channel);
                }
            }
        } catch (IOException | RuntimeException e) {
            LOGGER.warn("Unable to finish connection process", e);
            key.cancel();
            if (establishedTransfer != null) {
                establishedTransfer.closeChannels();
            }
        }
    }

    private void doPrematureRead(SelectionKey key) {
        TransferSide transfer = (TransferSide) key.attachment();
        if(!transfer.readData(stats)) {
            TransferSide.unregisterAll(transfer);
            transfer.closeChannels();
        } else if (transfer.getOtherSide().isBufferFull()) {
            key.cancel();
        }
    }
}