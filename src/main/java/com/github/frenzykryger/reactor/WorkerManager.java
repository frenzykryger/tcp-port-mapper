package com.github.frenzykryger.reactor;

import com.github.frenzykryger.configuration.Configuration;
import com.github.frenzykryger.helper.ConnectionManagementHelper;
import com.google.common.collect.Iterators;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Spawn workers and dispatches transfers for them.
 * Intended usage is to start worker threads using startWorkerManager() method
 * and then use obtained workerManager to dispatch transfer to workers.
 */
public final class WorkerManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkerManager.class);
    private final List<Queue<TransferSide>> incomingTransfers = new ArrayList<>();
    private final Iterator<Queue<TransferSide>> roundRobinIterator;
    private final Configuration configuration;
    private int cnt;

    /**
     * Spawn worker on each available core with account to hyper-threading
     *
     * @param configuration conf
     */
    public static WorkerManager startWorkerManager(Configuration configuration) {
        int workersCount = Runtime.getRuntime().availableProcessors();
        WorkerManager manager = new WorkerManager(workersCount, configuration);
        int i = 0;
        for (Queue<TransferSide> queue : manager.incomingTransfers) {
            Worker worker = new Worker(ConnectionManagementHelper.openSelector(), queue, configuration);
            new Thread(worker, "worker-" + i).start();
            ++i;
        }
        return manager;
    }

    private WorkerManager(int workersCount, Configuration configuration) {
        for (int i = 0; i < workersCount; ++i) {
            Queue<TransferSide> queue = new LinkedBlockingQueue<>(configuration.getPendingTransfersThreshold());
            incomingTransfers.add(queue);
        }
        this.roundRobinIterator = Iterators.cycle(incomingTransfers);
        this.configuration = configuration;
    }

    /**
     * Dispatch transfer to worker.
     * Transfer should not be modified after that by callee.
     * Should not be called from many threads.
     * Will discard transfers if queue is overflown.
     * Worker is guaranteed to see transfer in a provided state thanks to queue.
     *
     * @param transfer that have both connections established and is ready to transfer data
     */
    public void addTransfer(TransferSide transfer) {
        if (!roundRobinIterator.next().offer(transfer)) {
            transfer.closeChannels();
            if ((cnt++ % configuration.getDiscardedTransfersThreshold()) == 0) {
                LOGGER.warn("Discarded up to {} transfers due to queue overflow",
                        configuration.getDiscardedTransfersThreshold());
            }
        }
    }
}
