package com.github.frenzykryger;


import com.github.frenzykryger.configuration.Configuration;
import com.github.frenzykryger.configuration.PortMapping;
import com.github.frenzykryger.helper.ConfigurationHelper;
import com.github.frenzykryger.reactor.ConnectionManager;
import com.github.frenzykryger.reactor.WorkerManager;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class Main {
    /**
     * First arg is path to proxy.properties.
     * Additional configuration is obtained via system properties
     *
     * @param args maybe with alternate path to proxy.properties (./proxy.properties by default)
     * @throws IOException                by any number of reasons
     * @throws java.lang.RuntimeException when IOException is NOT enough
     */
    public static void main(String[] args) throws IOException {
        String propertiesPath = "proxy.properties";

        if (args.length >= 2) {
            propertiesPath = args[1];
        }

        FileInputStream propertiesStream = new FileInputStream(propertiesPath);

        Properties properties = new Properties();
        try {
            properties.load(propertiesStream);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Failed to load properties from path: %s", propertiesPath), e);
        }

        List<PortMapping> proxyConfiguration = ConfigurationHelper.createPortMapping(properties);
        Configuration configuration = new Configuration(System.getProperties());

        startProxy(proxyConfiguration, configuration);
    }

    /**
     * Starts a proxy
     *
     * @param proxyConfiguration port mappings as in proxy.properties
     * @param configuration      other configuration properties
     */
    public static void startProxy(List<PortMapping> proxyConfiguration, Configuration configuration) {
        WorkerManager workerManager = WorkerManager.startWorkerManager(configuration);
        ConnectionManager connectionManager = new ConnectionManager(proxyConfiguration, workerManager, configuration);

        connectionManager.acceptConnectionsIndefinitely();
    }
}
