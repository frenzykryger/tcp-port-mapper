package com.github.frenzykryger.helper;


import com.github.frenzykryger.configuration.PortMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Helper methods for property parsing
 */
public class ConfigurationHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationHelper.class);

    private static final String PROPERTY_FORMAT_ERROR = "Invalid property name: \"%s\". Proper format is: " +
            "<id>.[remoteHost|remotePort|localPort].";
    private static final String PORT_FORMAT_ERROR = "Invalid %s port number for %s: \"%s\". Should be integer.";
    private static final String NUMBER_FORMAT_ERROR = "Invalid number for %s: \"%s\". Should be integer.";
    private static final String REMOTE_HOST = "%s.remoteHost";
    private static final String REMOTE_PORT = "%s.remotePort";
    private static final String LOCAL_PORT = "%s.localPort";

    private static final Pattern RE_ID = Pattern.compile("(?<id>.*)\\.[^.]+");

    /**
     * Parse properties and return corresponding port mappings
     *
     * @param properties as specified in proxy.properties
     * @return list of port mappings
     */
    public static List<PortMapping> createPortMapping(Properties properties) {
        final List<PortMapping> portMapping = new ArrayList<>();
        final Set<String> ids = new HashSet<>();

        for (String name : properties.stringPropertyNames()) {
            Matcher matcher = RE_ID.matcher(name);
            if (!matcher.matches()) {
                throw new RuntimeException(String.format(PROPERTY_FORMAT_ERROR, name));
            }
            ids.add(matcher.group("id"));
        }

        for (String id : ids) {
            final String host = retrieveProperty(properties, String.format(REMOTE_HOST, id));
            final String remotePort = retrieveProperty(properties, String.format(REMOTE_PORT, id));
            final String localPort = retrieveProperty(properties, String.format(LOCAL_PORT, id));
            portMapping.add(createPortMapping(id, host, remotePort, localPort));
        }

        LOGGER.info("Recognized config: {}", portMapping);
        return portMapping;
    }

    /**
     * Parses number with meaningful error message in case of failure
     *
     * @param propertyName property name that has given value
     * @param value        that should be parsed to number
     * @return parsed number
     * @throws java.lang.RuntimeException if parsing fails
     */
    public static int parseNumber(String propertyName, String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new RuntimeException(String.format(NUMBER_FORMAT_ERROR, propertyName, value), e);
        }
    }

    private static String retrieveProperty(Properties properties, String name) {
        String property = properties.getProperty(name);
        if (property == null) {
            throw new RuntimeException(String.format("Missing property \"%s\" in config file", name));
        }
        return property;
    }

    private static PortMapping createPortMapping(String id, String host, String remotePortStr, String localPortStr) {
        int remotePort = retrievePort("remote", id, remotePortStr);
        int localPort = retrievePort("local", id, localPortStr);
        return new PortMapping(id, host, remotePort, localPort);
    }

    private static int retrievePort(String portDescription, String id, String port) {
        try {
            return Integer.parseInt(port);
        } catch (NumberFormatException e) {
            throw new RuntimeException(String.format(PORT_FORMAT_ERROR, portDescription, id, port), e);
        }
    }
}

