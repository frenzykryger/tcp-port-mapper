package com.github.frenzykryger.helper;

import com.github.frenzykryger.configuration.PortMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;

/**
 * Helper methods for managing channels and selectors
 */
public class ConnectionManagementHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionManagementHelper.class);

    /**
     * Accepts connection from ServerSocketChannel and sets accepted connection to non-blocking mode
     *
     * @param listeningChannel channel that awaits connection acceptance
     * @return SocketChannel as returned from @{link ServerSocketChannel.accept()}
     * @throws IOException if anything goes wrong
     */
    public static SocketChannel acceptConnection(ServerSocketChannel listeningChannel) throws IOException {
        try {
            SocketChannel acceptedChannel = listeningChannel.accept();
            if (acceptedChannel != null) {
                acceptedChannel.configureBlocking(false);
            }
            return acceptedChannel;
        } catch (IOException | RuntimeException e) {
            throw new IOException("Failed to accept connection", e);
        }
    }

    /**
     * Non-blockingly opens new channel
     *
     * @param portMapping port mapping with connection info (remote host and port)
     * @return opened channel
     * @throws IOException if anything goes wrong
     */
    public static SocketChannel openRemoteChannel(PortMapping portMapping) throws IOException {
        try {
            SocketChannel remoteChannel = SocketChannel.open();
            remoteChannel.configureBlocking(false);
            remoteChannel.connect(new InetSocketAddress(portMapping.getHost(), portMapping.getRemotePort()));
            return remoteChannel;
        } catch (IOException | RuntimeException e) {
            throw new IOException(String.format("Failed to initialize remote channel for mapping %s", portMapping), e);
        }
    }

    /**
     * Non-blockingly binds channels and registers them in provided selector using proxyConfiguration info
     *
     * @param connectionManagementSelector selector for channel registration with OP_ACCEPT
     * @param proxyConfiguration           info used for listening on ports (local ports)
     * @throws java.lang.RuntimeException if anything goes wrong
     */
    public static void listenOnPorts(Selector connectionManagementSelector, Iterable<PortMapping> proxyConfiguration) {
        LOGGER.info("Listening for new connections");
        for (PortMapping mapping : proxyConfiguration) {
            listenOnPort(connectionManagementSelector, mapping);
        }
    }

    /**
     * Non-blockingly binds channel and registers it in selector with OP_ACCEPT using provided PortMapping
     *
     * @param socketSelector selector for channel registration
     * @param portMapping    info used for listening on port (local port)
     * @return listening channel
     * @throws java.lang.RuntimeException if anything goes wrong
     */
    public static ServerSocketChannel listenOnPort(Selector socketSelector, PortMapping portMapping) {
        try {
            ServerSocketChannel localChannel = ServerSocketChannel.open();
            localChannel.configureBlocking(false);
            localChannel.bind(new InetSocketAddress(portMapping.getLocalPort()));
            localChannel.register(socketSelector, SelectionKey.OP_ACCEPT);
            return localChannel;
        } catch (IOException | RuntimeException e) {
            throw new RuntimeException(
                    String.format("Failed to initialize local channel for mapping %s", portMapping), e);
        }
    }

    /**
     * Opens new Selector with pray on epoll
     *
     * @return new Selector
     * @throws java.lang.RuntimeException if something goes wrong
     */
    public static Selector openSelector() {
        try {
            return SelectorProvider.provider().openSelector();
        } catch (IOException e) {
            throw new RuntimeException("Unable to open socket selector", e);
        }
    }

    /**
     * Try to close channel gracefully
     *
     * @param channel to be closed
     */
    public static void tryClose(SocketChannel channel) {
        try {
            channel.close();
        } catch (IOException e) {
            LOGGER.debug("Unable to properly close channel");
        }
    }
}
