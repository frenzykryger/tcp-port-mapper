package com.github.frenzykryger.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * Helper methods for reading and writing data between channel and buffer
 */
public class TransferHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(TransferHelper.class);

    /**
     * OK - data was read/written
     * SKIP - no data was transferred due to buffer overflow
     * CLOSE - underlying channel was gracefully or ungracefully closed
     * PARTIAL - not all data was transferred
     */
    public enum TransferResult {
        OK,
        SKIP,
        CLOSE,
        PARTIAL
    }

    /**
     * Reads data from channel to buffer.
     *
     * @param channel for data reading
     * @param buffer  for data receiving
     * @return OK|CLOSE|SKIP
     */
    public static TransferResult readData(SocketChannel channel, ByteBuffer buffer) {
        if (buffer.hasRemaining()) {
            try {
                int bytesRead = channel.read(buffer);

                if (bytesRead == -1) {
                    return TransferResult.CLOSE;
                } else if (bytesRead > 0) {
                    LOGGER.debug("Read {} bytes to {}", bytesRead, buffer);
                    return TransferResult.OK;
                }
            } catch (NullPointerException e) {
                throw e;
            } catch (IOException | RuntimeException e) {
                LOGGER.debug("Unable to read data from the channel", e);
                return TransferResult.CLOSE;
            }
        }
        return TransferResult.SKIP;
    }

    /**
     * Writes data from buffer to channel
     *
     * @param channel for writing
     * @param buffer  as data source
     * @return OK|PARTIAL|CLOSE|SKIP
     */
    public static TransferResult writeData(SocketChannel channel, ByteBuffer buffer) {
        buffer.flip();
        if (buffer.hasRemaining()) {
            try {
                int bytesWritten = channel.write(buffer);

                LOGGER.debug("{} bytes written on transfer", bytesWritten);
                if (!buffer.hasRemaining()) {
                    return TransferResult.OK;
                }
                if (bytesWritten > 0) {
                    return TransferResult.PARTIAL;
                }
            } catch (NullPointerException e) {
                throw e;
            } catch (IOException | RuntimeException e) {
                LOGGER.debug("Unable to write data in the channel", e);
                return TransferResult.CLOSE;
            } finally {
                if (buffer.hasRemaining()) {
                    buffer.compact();
                } else {
                    //buffer was written fully
                    buffer.clear();
                }
            }
        }
        return TransferResult.SKIP;
    }
}
