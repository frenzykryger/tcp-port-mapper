package com.github.frenzykryger.configuration;

import com.github.frenzykryger.helper.ConfigurationHelper;
import com.github.frenzykryger.reactor.ByteBufferPool;
import org.apache.commons.lang3.BooleanUtils;

import java.util.Properties;

/**
 * Thread-safe holder for system configuration properties:
 * max.transfers.chunk: Max number of connections that each worker accepts after waking up (defaults to 25)
 * max.buffer.size: Max buffer size for each SocketChannel (defaults to 30K)
 * max.concurrent.connections: Max numbers of concurrent connections (defaults to 20000).
 * ${max.buffer.size} * ${max.concurrent.connections} * 2 gives necessary memory amount for buffers in native heap.
 * even if not all available data was written to it (defaults to false)
 * worker.idle.timeout.millis: Max worker idle time before he tries to accept new transfers
 * discarded.transfers.threshold: Number of transfers that should be discarded before warning message will reappear
 * (defaults to 100)
 * pending.transfers.threshold: If number of transfers reaches this size,
 * connections will be discarded and removed from queue
 * stats.period: Number of selects after workers will report their statistics
 * enable.speculative.writes: If true, proxy will try to write data right after read. As channels are typically
 * ready for write this can improve performance because there will be no select() induced penalty
 * when proxy just holds it's bytes (defaults to true)
 */
public class Configuration {
    public static final String MAX_TRANSFERS_CHUNK = "max.transfers.chunk";
    public static final String MAX_BUFFER_SIZE = "max.buffer.size";
    public static final String MAX_CONCURRENT_CONNECTIONS = "max.concurrent.connections";
    public static final String WORKER_IDLE_TIMEOUT_MILLIS = "worker.idle.timeout.millis";
    public static final String STATS_PERIOD = "stats.period";
    public static final String DISCARDED_TRANSFERS_THRESHOLD = "discarded.transfers.threshold";
    public static final String PENDING_TRANSFERS_THRESHOLD = "pending.transfers.threshold";
    public static final String SPECULATIVE_WRITES = "enable.speculative.writes";
    public static final String DEFAULT_MAX_TRANSFERS_CHUNK = "25";
    public static final String DEFAULT_WORKER_IDLE_TIMEOUT_MILLIS = "100";
    public static final String DEFAULT_MAX_BUFFER_SIZE = "30720";
    public static final String DEFAULT_MAX_CONCURRENT_CONNECTIONS = "20000";
    public static final String DEFAULT_SPECULATIVE_WRITES = "true";
    public static final String DEFAULT_DISCARDED_TRANSFERS_THRESHOLD = "100";
    public static final String DEFAULT_PENDING_TRANSFERS_THRESHOLD = "100000";
    public static final String DEFAULT_STATS_PERIOD = "10000";

    private final int maxTransfersChunk;
    private final long maxWorkerIdleTimeout;
    private final long discardedTransfersThreshold;
    private final boolean speculativeWrites;
    private final int pendingTransfersThreshold;
    private final int statsPeriod;
    private final ByteBufferPool byteBufferPool;

    /**
     * Constructor that retrieves configuration from properties
     *
     * @param properties properties
     */
    public Configuration(Properties properties) {
        this.maxTransfersChunk = ConfigurationHelper.parseNumber(MAX_TRANSFERS_CHUNK,
                properties.getProperty(MAX_TRANSFERS_CHUNK, DEFAULT_MAX_TRANSFERS_CHUNK));
        int maxBufferSize = ConfigurationHelper.parseNumber(MAX_BUFFER_SIZE,
                properties.getProperty(MAX_BUFFER_SIZE, DEFAULT_MAX_BUFFER_SIZE));
        int maxConcurrentConnections = ConfigurationHelper.parseNumber(MAX_CONCURRENT_CONNECTIONS,
                properties.getProperty(MAX_CONCURRENT_CONNECTIONS, DEFAULT_MAX_CONCURRENT_CONNECTIONS));

        this.byteBufferPool = new ByteBufferPool(maxBufferSize, maxConcurrentConnections * 2);

        this.speculativeWrites = BooleanUtils.toBoolean(properties.getProperty(SPECULATIVE_WRITES, DEFAULT_SPECULATIVE_WRITES),
                "true", "false");
        this.maxWorkerIdleTimeout = ConfigurationHelper.parseNumber(WORKER_IDLE_TIMEOUT_MILLIS,
                properties.getProperty(WORKER_IDLE_TIMEOUT_MILLIS, DEFAULT_WORKER_IDLE_TIMEOUT_MILLIS));
        this.discardedTransfersThreshold = ConfigurationHelper.parseNumber(DISCARDED_TRANSFERS_THRESHOLD,
                properties.getProperty(DISCARDED_TRANSFERS_THRESHOLD, DEFAULT_DISCARDED_TRANSFERS_THRESHOLD));
        this.pendingTransfersThreshold = ConfigurationHelper.parseNumber(PENDING_TRANSFERS_THRESHOLD,
                properties.getProperty(PENDING_TRANSFERS_THRESHOLD, DEFAULT_PENDING_TRANSFERS_THRESHOLD));
        this.statsPeriod = ConfigurationHelper.parseNumber(STATS_PERIOD,
                properties.getProperty(STATS_PERIOD, DEFAULT_STATS_PERIOD));
    }

    /**
     * @return see class doc
     */
    public int getMaxTransfersChunk() {
        return maxTransfersChunk;
    }

    /**
     * Maybe spring isn't overkill here after all
     */
    public ByteBufferPool getByteBufferPool() {
        return byteBufferPool;
    }

    /**
     * @return See class doc
     */
    public long getMaxWorkerIdleTimeout() {
        return maxWorkerIdleTimeout;
    }

    /**
     * @return See class doc
     */
    public long getDiscardedTransfersThreshold() {
        return discardedTransfersThreshold;
    }

    /**
     * @return See class doc
     */
    public int getPendingTransfersThreshold() {
        return pendingTransfersThreshold;
    }

    /**
     * @return See class doc
     */
    public int getStatsPeriod() {
        return statsPeriod;
    }

    /**
     * @return See class doc
     */
    public boolean isSpeculativeWritesEnabled() {
        return speculativeWrites;
    }
}
