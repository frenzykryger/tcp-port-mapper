package com.github.frenzykryger.configuration;

/**
 * Immutable port mapping representing one proxy path
 */
public class PortMapping {
    private final String id;
    private final String host;
    private final int remotePort;
    private final int localPort;

    public PortMapping(String id, String host, int remotePort, int localPort) {
        this.id = id;
        this.host = host;
        this.remotePort = remotePort;
        this.localPort = localPort;
    }

    public String getHost() {
        return host;
    }

    public int getRemotePort() {
        return remotePort;
    }

    public int getLocalPort() {
        return localPort;
    }

    @Override
    public String toString() {
        return '\'' + id + "' -> {" +
                "host='" + host + '\'' +
                ", remotePort='" + remotePort + '\'' +
                ", localPort='" + localPort + '\'' +
                '}';
    }
}
