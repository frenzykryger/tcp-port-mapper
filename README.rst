tcp-port-mapper
===============

Requires
--------
java.version >= 7


Build::

        mvn clean install


Launch::

        java -jar target/tcp-port-mapper-jar-with-dependencies.jar [/path/to/proxy.properties]


Configuration
-------------

Via system properties:

 - max.transfers.chunk: Max number of connections that each worker accepts after waking up (defaults to 25)

 - max.buffer.size: Max buffer size for each SocketChannel (defaults to 30K)

 - max.concurrent.connections: Max numbers of concurrent connections (defaults to 20000). ${max.buffer.size} × ${max.concurrent.connections} × 2 gives necessary memory amount for buffers in native heap.

 - enable.speculative.writes: If true, proxy will try to write data right after read. As channels are typically ready for write this can improve performance because there will be no select() induced penalty when proxy just holds it's bytes (defaults to true)

 - worker.idle.timeout.millis: Max worker idle time before he tries to accept new transfers (100ms by default)

 - discarded.transfers.threshold: Number of transfers that should be discarded due to queue overflow before warning message will reappear (defaults to 100)

 - pending.transfers.threshold: Worker's queue size limit (100000 items by default)

 - stats.period: Number of selects after workers will report their statistics (10000 selects by default)



.. image:: https://bitbucket.org/frenzykryger/tcp-port-mapper/raw/master/reactor.png
